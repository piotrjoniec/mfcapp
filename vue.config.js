var manifestJSON = require('./public/manifest.json');

module.exports = {
    pwa: {
      workboxPluginMode: 'InjectManifest',
      workboxOptions: {
        swSrc: 'src/sw.js',
      },
      themeColor: manifestJSON.theme_color,
      msTileColor: manifestJSON.background_color
    }
  }