import HomeScreen from './views/HomeScreen.vue';
import AboutScreen from './views/AboutScreen.vue';
import CarScreen from './views/CarScreen.vue';
import NewsSubScreen from './views/NewsSubScreen.vue';

export default [
    {path: '/', component: HomeScreen, name: 'HomeScreen'},
    {path: '/about', component: AboutScreen, name: 'AboutScreen'},
    {path: '/car/:slug', component: CarScreen, name: 'CarScreen'},
    {path: '/news-subscription', component: NewsSubScreen, name: 'NewsSubScreen'},
];