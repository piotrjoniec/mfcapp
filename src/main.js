import './config';

import Vue from 'vue';
import VueRouter from 'vue-router';
import _ from 'lodash';
import './plugins/swiper';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import vuetify from './plugins/vuetify';
import App from './App.vue';
import routes from './routes';
import axios from 'axios';
window.axios = axios;

import './registerServiceWorker';
import './scss/main.scss';

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(VueAwesomeSwiper);

const router = new VueRouter({ routes, mode: 'history' });

new Vue({
  render: h => h(App),
  vuetify,
  router
}).$mount('#app');
