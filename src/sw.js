// push message handler
self.addEventListener('push', function(event) {
  if (event.data) {
    const pushObj = event.data.json();
    if(pushObj.text && pushObj.link) {
      event.waitUntil(self.registration.showNotification(pushObj.text, {data: pushObj.link}));
    }
  } else {
    // no data received
  }
});

// notification click handler
self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  event.waitUntil(
    clients.openWindow(event.notification.data)
  );
});

// speed up service worker installation
self.addEventListener('install', function(event) {
  event.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', function(event) {
  event.waitUntil(self.clients.claim());
});

// cache stuff
if(workbox) {
    // register auto generated manifest
    workbox.precaching.precacheAndRoute(self.__precacheManifest);

    // register main navigation route (fallback for offline browsing)
    workbox.routing.registerNavigationRoute('/index.html');

    // api caching
    workbox.routing.registerRoute(
        new RegExp('https://mfcapp.webdevstudio.nl/api/.+'),
        new workbox.strategies.NetworkFirst({
            cacheName: 'api-cache',
            plugins: [
                new workbox.cacheableResponse.Plugin({
                  headers: {
                    'X-Is-Cacheable': 'true',
                  },
                })
            ]
        })
    );

    // image caching
    workbox.routing.registerRoute(
        new RegExp('/img/'),
        new workbox.strategies.CacheFirst({
          cacheName: 'image-cache',
          plugins: [
            new workbox.expiration.Plugin({
              maxAgeSeconds: 30 * 24 * 60 * 60,
              maxEntries: 50,
            }),
          ]
        })
    );
}